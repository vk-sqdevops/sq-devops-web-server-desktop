resource "aws_alb" "web-alb" {
  name                = "${var.deployment-prefix}-web-alb"
  internal            = false
  idle_timeout        = "300"
  security_groups     = ["${var.web-server-sg-id}"]
  subnets             = ["${local.public-subnets}"] 
  
  tags {
    Name            = "${var.deployment-prefix}-web-alb"
    Organization    = "${var.organization}"
    Project         = "${var.project}"
    Environment     = "${var.environment}"
  }
}

resource "aws_alb_listener" "alb-listener-https" {
  load_balancer_arn = "${aws_alb.web-alb.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${aws_iam_server_certificate.web-cert.arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.web-tg.arn}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "alb-listener-http" {
  load_balancer_arn = "${aws_alb.web-alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.web-tg.arn}"
    type             = "forward"
  }
}