resource "aws_iam_server_certificate" "web-cert" {
  name                = "${var.deployment-prefix}-web-cert"
  certificate_body    = "${file("./certs/squirt.org.cert")}"
  private_key         = "${file("./certs/squirt.org.key")}"
}