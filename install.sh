#!/bin/bash

export PATH=$PATH:/opt/terraform &&
terraform init &&

# Argument/Env Variable Verification
if [ \( -z "$AWS_ACCESS_KEY_ID" \) -a \( $# -ne 4 \) ]
  then
    echo -e "Invalid number of arguments/environment variables \n
      1) AWS_ACCESS_KEY_ID \n
      2) AWS_SECRET_ACCESS_KEY \n
      3) AWS_DEFAULT_REGION \n
      4) BUCKET_NAME \n
      Example: ./install.sh ${AWS_ACCESS_KEY_ID} ${AWS_SECRET_ACCESS_KEY} ${AWS_DEFATUL_REGION} ${BUCKET_NAME} \n"
    exit
elif [ $# -eq 4 ]
    then
      export AWS_ACCESS_KEY_ID=$1
      export AWS_SECRET_ACCESS_KEY=$2
      export AWS_DEFAULT_REGION=$3
      export BUCKET_NAME=$4
fi

# Download settings and terraform state for existing deployment - may fail for new deployment
aws s3 cp s3://$BUCKET_NAME/settings.json . &&
aws s3 cp s3://$BUCKET_NAME/web-desktop/terraform.tfstate .&&

echo "Running Terraform Templates..."

terraform apply \
  -var "aws-access-key=$AWS_ACCESS_KEY_ID" \
  -var "aws-secret-key=$AWS_SECRET_ACCESS_KEY" \
  -var "aws-region=$AWS_DEFAULT_REGION" \
  -var "s3-bucket-name=$BUCKET_NAME" \
  -var-file="settings.json" \
  -auto-approve &&

# Upload tf state
aws s3 cp ./terraform.tfstate s3://$BUCKET_NAME/web-desktop/terraform.tfstate

# Update and upload settings
jq --arg value $(terraform output web-alb-dns) '."web-alb-dns" = $value' < ./settings.json | cat | tee ./settings-tmp.json
mv -f ./settings-tmp.json ./settings.json
aws s3 cp ./settings.json s3://$BUCKET_NAME/settings.json &&
rm -f ./settings.json

echo "Terraform apply is complete!"
echo "Web Site Url: https://$(terraform output web-alb-dns)"

