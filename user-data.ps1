<persist>true</persist>
<powershell>
$bucketName = "${s3-bucket-name}"
$rootPath = "C:\\Squirt\\"
$scriptPath ="$rootPath\\Scripts\\"
$settingsFileName = "settings.json"
$webConfigPath = "$rootPath\\Web\\web.config"

Copy-S3Object -BucketName $bucketName -key $settingsFileName -file $scriptPath\$settingsFileName

$settingsJson = Get-Content("$scriptPath\$settingsFileName") | Out-String | ConvertFrom-Json
$sqlServer = $settingsJson."sql-server-dns"
$sqlDbName = "SquirtPrimary"
$sqlUid = $settingsJson."sql-web-username"
$sqlPwd = $settingsJson."sql-web-password"

$adoConnectionString = "Provider=SQLOLEDB;Data Source=$sqlServer;Initial Catalog=$sqlDbName;User ID=$sqlUid; Password=$sqlPwd;"
$dotNetConnectionString = "Server=$sqlServer;Database=$sqlDbName;User ID=$sqlUid;Password=$sqlPwd;"

$redisAuthToken=$settingsJson."redis-server-password"
$redisAddress=$settingsJson."redis-server-address"
$redisPort=$settingsJson."redis-server-port"
$redisConnectionString="$redisAddress" + ":" + "$redisPort" + ",password=$redisAuthToken"

###########################################################################################################################
# Update registry
###########################################################################################################################
$registryPath='HKEY_LOCAL_MACHINE\SOFTWARE\Squirt35'

New-ItemProperty -Path Registry::$registryPath -Name "ConnectionStringPrimary" -Value $adoConnectionString -Force
New-ItemProperty -Path Registry::$registryPath -Name "NETConnectionStringPrimary" -Value $dotNetConnectionString -Force
New-ItemProperty -Path Registry::$registryPath -Name "RedisSessionConnectionString" -Value $redisConnectionString -Force

$registryPath='HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\SQUIRT35'

New-ItemProperty -Path Registry::$registryPath -Name "ConnectionStringPrimary" -Value $adoConnectionString -Force
New-ItemProperty -Path Registry::$registryPath -Name "NETConnectionStringPrimary" -Value $dotNetConnectionString -Force
New-ItemProperty -Path Registry::$registryPath -Name "RedisSessionConnectionString" -Value $redisConnectionString -Force
###########################################################################################################################

###########################################################################################################################
# Update web.config
###########################################################################################################################
[xml]$webConfigXml = Get-Content -Path $webConfigPath

$key = "caching:redisCacheServer"
[System.Xml.XmlAttribute]$node = $webConfigXml.selectNodes("/configuration/appSettings/add[@key='$key']/@value")[0]
$node.InnerText = "$redisAddress"

$key = "caching:redisCacheServerPort"
[System.Xml.XmlAttribute]$node = $webConfigXml.selectNodes("/configuration/appSettings/add[@key='$key']/@value")[0]
$node.InnerText = "$redisPort"

$key = "caching:redisCacheServerPassword"
[System.Xml.XmlAttribute]$node = $webConfigXml.selectNodes("/configuration/appSettings/add[@key='$key']/@value")[0]
$node.InnerText = "$redisAuthToken"

$webConfigXml.Save($webConfigPath)
###########################################################################################################################

###########################################################################################################################
# Grant Registry permissions to IUSR
###########################################################################################################################
$inherit = [system.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
$propagation = [system.security.accesscontrol.PropagationFlags]"None"
$rule = new-object system.security.accesscontrol.registryaccessrule "IUSR","FullControl",$inherit,$propagation,"Allow"

$registryPath = "HKLM:\\SOFTWARE\\Squirt35"
$acl = Get-Acl $registryPath
$acl.SetAccessRule($rule)
$acl | Set-Acl -Path $registryPath

$registryPath = "HKLM:\\SOFTWARE\\WOW6432Node"
$acl = Get-Acl $registryPath
$acl.SetAccessRule($rule)
Set-Acl -Path $registryPath -AclObject $acl

New-PSDrive HKU Registry HKEY_USERS
$registryPath = "HKU:\\.DEFAULT"
$acl = Get-Acl $registryPath
$acl.SetAccessRule($rule)
Set-Acl -Path $registryPath -AclObject $acl
###########################################################################################################################
</powershell>