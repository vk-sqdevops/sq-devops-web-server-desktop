resource "aws_alb_target_group" "web-tg" {
  name              = "${var.deployment-prefix}-web-tg"
  port              = 80
  protocol          = "HTTP"
  vpc_id            = "${var.vpc-id}"
  
  tags {
    Name            = "${var.deployment-prefix}-web-tg"
    Organization    = "${var.organization}"
    Project         = "${var.project}"
    Environment     = "${var.environment}"
  }
}

resource "aws_alb_target_group_attachment" "web-tg-attachment" {
  target_group_arn = "${aws_alb_target_group.web-tg.arn}"
  target_id = "${aws_instance.web-server-desktop.id}"
  port = 80
}